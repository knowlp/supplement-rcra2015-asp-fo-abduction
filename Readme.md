
# Supplementary Data Repository RCRA 2015

This is the supplementary data repository for the publication 
**Modeling Abduction over Acyclic First-Order Logic Horn Theories in Answer Set Programming: Preliminary Experiments**
by **Peter Schüller**
at the *22nd RCRA International Workshop on "Experimental Evaluation of Algorithms for solving problems with combinatorial explosion"* (RCRA2015).

The repository contains encodings for all experiments reported in the paper.

 * Each encoding is contained in a separate file.
 * Encodings for the 5 different formulations are contained in folders backch-a/ backch-b/ backch-c/ bwfw/ simpl/

